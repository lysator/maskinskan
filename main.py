#!/usr/bin/env python3

"""
Full script, which generates the final report.

This program is still very much a work in progress, but the general
flow is detailed below.
"""

import subprocess
import xml.etree.ElementTree as ET
import requests
import aiohttp
import asyncio
from urllib.parse import urlencode

def get_dns_stuff():
    # cmd = subprocess.run(["guile", "./lysator.scm"], text=True, capture_output=True, check=True)
    cmd = subprocess.run(["cat", "lysator.xml"],
                        text=True,
                        capture_output=True,
                        check=True)
    root = ET.fromstring(cmd.stdout)


    for record in root:
        print(record.attrib)
        # record.attrib['last-octet']
        # record.attrib.get('reverse')
        # record.attrib.get('addr-4')
        # record.attrib.get('addr-6')

def get_nmap_stuff():
    pass


async def get_mediawiki_stuff(reverse_results: list[str]):
    async with aiohttp.ClientSession() as session:
        for hostname in reverse_results:
            urlencode({
                'action': 'parse',
                'format': 'json',
                'page': hostname,
                'prop': 'text',
            })
            async with session.get(url) as response:
                pass



# list of dictionaries, at least including last-octet
dns_stuff = get_dns_stuff()

# for each dns stuff
#    compare expected octet with forward ipv4 and ipv6

# for each dns stuff
#    if it has reverse
#        get datorhandbok

nmap_stuff = get_nmap_stuff()

# for each machine in [0..0x100)
#     finns reverse för addressen
#         (på ipv4)
#         (på ipv6)
#     har den (rimlig) ansvarig root i datorhandboken
#     är "Status" i datorhandboken rimlig
#     har den SSH-fingeravtryck i datorhandboken?
#     om så:
#           jämför med dem från nmap
#           verifiera signatur



