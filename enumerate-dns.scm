;;; Commentary:
;;; Goes through all Lysator machines (as found by enumerating IP
;;; addresses from 0 to 255). Currently does a reverse lookup on the
;;; IPv4 address, and then goes forward lookups for IPv4 and IPv6
;;; addresses.
;;;
;;; An xml report is printed to stdout.
;;; Code:

(use-modules (dns)
             ((dns reverse) :select (reverse-address))
             (rnrs bytevectors)
             ((sxml simple) :select (sxml->xml)))


;; Procedure which returns an id number, to be used with DNS queries.
(define get-id
  (let ((i 100))
    (lambda () (set! i (1+ i)) i)))

;; Creates and connects an UDP socket on the DNS port for the given
;; address.
(define (setup-sock addr)
  (define ai (car (getaddrinfo addr "domain" 0 0 SOCK_DGRAM)))
  (define sock (socket (addrinfo:fam ai)
                       (addrinfo:socktype ai)
                       (addrinfo:protocol ai)))
  (connect sock (addrinfo:addr ai))
  sock)

;;; Sends a DNS message over socket, awaits a response, and then
;;; return that (parsed) object.
(define (run sock msg)
  (define resp (make-bytevector 512))
  (send sock (pack-dns-message msg))
  (recv! sock resp)
  (unpack-dns-message resp))



;; Connection to our DNS server
;; NOTE may be configured
(define sock (make-parameter (setup-sock "8.8.8.8")))

;; Utility procedure for looking up DNS records.
;; Addr is the string to lookup (e.g. "www.example.com"), and type is
;; either an upper case symbol matching the type name (A, AAAA, ...),
;; or the actuall numerical value. See guile-dns for further
;; documentation
(define (resolve addr type)
  (define msg
    (make-dns-message
     header: (make-dns-header id: (get-id) rd: #t qdcount: 1)
     questions: (list (make-dns-question
                       type: type
                       name: addr))))
  (run (sock) msg))

;;; Looks up a hostname from an address.
(define (resolve-reverse addr)
  (resolve (reverse-address addr) 'PTR))



(define (add! responses i key value)
  (vector-set! responses i
               (acons key value (vector-ref responses i))))

(define (add-addresses! responses)
 (map (lambda (i)
        (define response (resolve-reverse (format #f "130.236.254.~a" i)))
        (format (current-error-port) "\r~a/256" i)
        ;; TODO errors here?
        (unless (null? (answers response))
          (let ((addr (rdata (car (answers response)))))
            (add! responses i 'reverse addr)
            (let ((proc (compose rdata car answers))
                  (test (compose (negate null?) answers)))
              (cond ((resolve addr 'A) test
                     => (lambda (resp) (add! responses i 'addr-4 (proc resp)))))
              (cond ((resolve addr 'AAAA) test
                     => (lambda (resp) (add! responses i 'addr-6 (proc resp))))))
            )))
      (iota 256)))


(define (build-xml-document responses)
  `(records
    ,@(map (lambda (record)
             `(record
               (@ (last-octet ,(assoc-ref record 'base))
                  ,@(cond ((assoc-ref record 'reverse)
                           => (lambda (it) `((reverse ,it))))
                          (else '()))
                  ,@(cond ((assoc-ref record 'addr-4)
                           => (lambda (it) `((addr-4 ,it))))
                          (else '()))
                  ,@(cond ((assoc-ref record 'addr-6)
                           => (lambda (it) `((addr-6 ,it))))
                          (else '())))))
           (vector->list responses))))


;;; This is a vector of alists, where each alist holds resolved
;;; records for that address.
;;; The keys which may be present are:
;;; base :: The last octet of the address (always present)
;;; reverse :: result of a reverse lookup on the address (a hostname)
;;; addr-4 :: IPv4 address from looking up the hostname found from reverse
;;; addr-6 :: IPv6 --||--

(define responses (make-vector 256))
(for-each (lambda (i) (vector-set! responses i `((base . ,i)))) (iota 256))

(add-addresses! responses)

(sxml->xml
 `(*TOP*
   (*PI* xml "version=\"1.0\" encoding=\"UTF-8\"")
   ,(build-xml-document responses)))
