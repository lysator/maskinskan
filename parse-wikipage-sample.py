#!/usr/bin/env python3

"""
Sample for parsing tables from Mediawiki.

Takes an rendered mediawiki-page (as written by
get-wikipage-sample.sh), and extracts the key-value table from the
about the machine.
"""

from xml.dom.minidom import parseString, Element

with open('Slartibartfast.html') as f:
    data = f.read()

dom = parseString(data)


def inner_text(el: Element) -> str:
    if el.nodeType == Element.TEXT_NODE:
        return el.data
    else:
        return ''.join(inner_text(c) for c in el.childNodes)


tbody = dom.getElementsByTagName('tbody')[0]

print({
    inner_text(tr.getElementsByTagName('th')[0]).strip():
    inner_text(tr.getElementsByTagName('td')[0]).strip()
    for tr
    in tbody.getElementsByTagName('tr')})
