#!/bin/bash

# Utkast på skript för att ladda ner Datorhandbokssidor via
# Mediawikis API.

api_base='https://datorhandbok.lysator.liu.se/api.php'

get() {
	page=$1
	prop=$2
	set -x
	curl "${api_base}?action=parse&format=json&page=${page}&prop=${prop}" 
}

page=Slartibartfast

get "$page" text     | jq --raw-output '.parse.text."*"'     > "$page.html"
get "$page" wikitext | jq --raw-output '.parse.wikitext."*"' > "$page.wiki"

