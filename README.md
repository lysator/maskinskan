Skanning av Lysators maskiner
=============================

Programmets mål är att skanna igenom alla Lysators maskiner på olika sätt, och
generera en rapport om vad som finns, vad som påstår sig finnas, och hur bra
dessa stämmer överens med varandra.

Som grund kommer alla IP-addresser i rymden 130.236.254.0/24 mappas, och
refereras till med dess sista oktet.

Slutrapporten skall (åtminstone) innehålla

- sista okteten
- Reverse IPv4 
- Reverse IPv6 
- Forward IPv4 från Reverse IPv4 
- Forward IPv6 från Reverse IPv4 
- Forward IPv4 från Reverse IPv6 
- Forward IPv6 från Reverse IPv6 
- ansvarig datorhandbok (färglägg rött om gamling)
- status i datorhandbok (markera om står som "ur drift" men svarar, och vice versa)
- har den ssh-nycklar i datorhandboken
- Är det samma nycklar i datorhandboken som maskinen svarar med
    - Extra nycklar
    - Saknade nycklar
    - Diffande nycklar
- Är nycklarna signerade av root

Med framtida tilläggen

- Är maskinen fysisk eller virtuell?
- Om den är virtuell, finns den i proxmox?
- Finns maskiner i proxmox som inte finns i datorhandboken?

Rapport ska skapas innnifrån och utifrån Lysators nät.
